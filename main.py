from fastapi import FastAPI,Request
import tagger
import timeit
app = FastAPI()
tag = tagger.Tagger()

print('\nLoading Data')
t1 = timeit.default_timer()
#tag.load_names('./data/feiba-entities.tsv', './data/feiba-names.tsv')
tag.load_names('./data/tagger_entities.tsv', './data/tagger_names.tsv')
t2 = timeit.default_timer() - t1
print('\nData Loaded Succesfully in %f Seconds' %t2)



@app.get("/")
async def root(s:str):
    matches = tag.get_matches(s, '01' ,[-1,9606] )
    print(matches)
    return {"Matches": matches}
